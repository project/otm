/* Office time management is developer for log the employes working time and to log the work details.
it includes the half day management and eairly leave also.
*/

1. Install or download module and make sure to place in "/sites/all/modules" directory.

2. Download jQuery update module from "https://www.drupal.org/project/jquery_update".

3. You will get configuration in "admin/config/employee/setting".

4. set your hours of "full day", "half day", and number of "eairly leave".

5. Go to "/employee/timetracker" , click "Checkin" button from when you want to start your working time.
(it will take automatically the current time as your checkin time in "Office In Time" field on the same page).

6. "Work Update" will be use when you want to submit the all your work update and you want to leave the office.

7. The submit button will enable when it completes the "Half Day Time" which you have set.

8. In case of Full day, when you complete the "Full Day Time" which you have set the message will show as green.

9. Add your work update and submit. On click submit button it will automatically log your office out time.

10. To check your work status go to "/employee/timesheet" . here you will find all details of your work.
