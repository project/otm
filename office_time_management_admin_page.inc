<?php
function office_time_management_admin_setting_form() {
	$halfday_time = '04:30';
	$fullday_time = '08:00';
	$early_leave_day = 2;
	$settingcount = db_select('employee_admin_setting', 'eas');
	$settingcount->fields('eas', ['id', 'halfday_time', 'fullday_time', 'early_leave_day']);
	$result = $settingcount->execute();
    $settingcountdata = $result->fetchAll();
    foreach ($settingcountdata as $value) {
    }
    if(!empty($value->halfday_time)){
		$halfday_time = $value->halfday_time;
		$fullday_time = $value->fullday_time;
		$early_leave_day = $value->early_leave_day;
	}
	
	$form['halfday_time']=array(
		'#type'=>'textfield',
		'#title'=>t('Half Day Time'),
		'#description'=>t('Please insert your Half Day time. Ex: 04:30'),
		'#required' => TRUE,
		'#default_value' => $halfday_time,
		'#attributes' => array('class' => array('emp-halfday_time'),
							),
	);
	$form['fullday_time']=array(
		'#type'=>'textfield',
		'#title'=>t('Full Day Time'),
		'#description'=>t('Please insert your Full Day time. Ex: 08:30'),
		'#required' => TRUE,
		'#attributes' => array('class' => array('emp-fullday_time'),
							),
		'#default_value' => $fullday_time,
	);	
	$form['early_leave_day']=array(
		'#type'=>'textfield',
		'#title'=>t('Early Leave Day'),
		'#description'=>t('Please insert No of day early leave. Ex: 2'),
		'#required' => TRUE,
		'#attributes' => array('class' => array('emp-early_leave_day'),
							),
		'#default_value' => $early_leave_day,
	);	
	$form['submit']=array(
		'#type'=>'submit',
		'#value'=>t('Submit'),
		'#attributes' => array('class' => array('emp-submit-checkin'),
							   'id' => 'checkin_submit'
							)
	);
	return $form;
}

function office_time_management_admin_setting_form_submit($form, &$form_state) {

	$data = array(
		'halfday_time' => $form_state['input']['halfday_time'],
		'fullday_time' => $form_state['input']['fullday_time'],
		'early_leave_day' => $form_state['input']['early_leave_day'],
	);	
	
	$count = db_select('employee_admin_setting', 'eas')
			->fields('eas', array('id'))
			->execute()->fetchCol();

	if (!empty($count)) { 			
		$query = db_update('employee_admin_setting')
			->fields(array(
				'halfday_time' => $data['halfday_time'],
				'fullday_time' => $data['fullday_time'],
				'early_leave_day' => $data['early_leave_day'],
			))
			->condition('id', $count[0])
			->execute();
		drupal_set_message('Employee admin setting successfully Updated', $type = 'status');
	}
	else{
		$query = db_insert('employee_admin_setting')->fields($data);
			$submit_query = $query->execute();
		drupal_set_message('Employee admin setting successfully saved', $type = 'status');
	}
	
}

