<?php

function office_time_management_ajax_data(){
	$current_time = date("h:i a");
	$emp_checkin_time = date("h:i a");
	global $user;
	$update_date = date("d-M-Y");
	$emp_checkin_result = db_select('employee_checkin', 'ec')
			->fields('ec', array('office_checkin_time'))
			->condition('employee_id', $user->uid, '=')
			->condition('update_date', $update_date, '=')
			->execute()->fetchCol();
	if (!empty($emp_checkin_result)) { 
		$emp_checkin_time = $emp_checkin_result[0];
	}

	$intime = strtotime($emp_checkin_time);
	$outtime = strtotime($current_time);
	
	$timedif = ($outtime - $intime) / 60;
	$hour =  floor($timedif / 60);
	$minute = ($timedif % 60);
	
	$completed_time = sprintf('%02d:%02d', $hour, $minute);
	//~ $finalcompleted_time = strtotime($completed_time);
	//~ print $completed_time;
	
	
	$datas['msg'] = 'Success';
	$datas['current_time'] = $current_time;
	$datas['emp_checkin_time'] = $emp_checkin_time;
	$datas['completed_time'] = $completed_time;
	print_r(json_encode($datas));
	exit;
}
